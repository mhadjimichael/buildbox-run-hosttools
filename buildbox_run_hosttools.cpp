/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_runner.h>

#include <stdlib.h>
#include <unistd.h>

using namespace buildboxcommon;

/**
 * The simplest possible working runner: runs a job directly on your machine
 * as the curent user without making any attempt to sandbox it.
 */
class HostToolsRunner : public Runner {
  public:
    ActionResult execute(const Command &command,
                         const Digest &inputRootDigest) override
    {
        auto stagedDir = this->stage(inputRootDigest);
        const std::string workingDir = stagedDir->getPath() +
                                       std::string("/") +
                                       command.working_directory();

        // Change working directory.
        chdir(workingDir.c_str());

        // Setup environement from the Command variables
        for (const auto envVar : command.environment_variables()) {
            if (setenv(envVar.name().c_str(), envVar.value().c_str(), 1) ==
                -1) {
                throw std::system_error(errno, std::system_category());
            }
        }

        // Create parent directories for the Command's output files
        for (const std::string file : command.output_files()) {
            if (file.find("/") != std::string::npos) {
                FileUtils::create_directory(
                    (workingDir + file.substr(0, file.rfind("/"))).c_str());
            }
        }

        // Append build command.
        std::vector<std::string> commandLine;
        for (const std::string argument : command.arguments()) {
            commandLine.push_back(argument);
        }

        ActionResult result;
        executeAndStore(commandLine, &result);

        stagedDir->captureAllOutputs(command, &result);
        return result;
    }
};

BUILDBOX_RUNNER_MAIN(HostToolsRunner);
